import React,{ useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { FlatList } from "react-native";

const styles = StyleSheet.create({
    0: {
        color: "yellow",
        fontSize: 20
    },
    1: {
        color: "silver",
        fontSize: 20
    },
    2: {
        color: "brown",
        fontSize: 20
    },
    "2row": {
        borderBottomColor: 'gray',
        borderBottomWidth: 1
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 5
    },
    container: {
        margin: 40
    },
    text: {
        fontFamily: 'sans-serif'
    }
});

const Score = ({items}) => {
    return(
        <View style={styles.container}>
            <FlatList
                style={{flex: 1}}
                data={items.sort((a, b) => {
                    return a.score - b.score;
                }).reverse()}
                keyExtractor={item => item.id}
                renderItem={({item, index}) => {
                    return(
                        <View style={[styles[index + "row"], styles.row]}>
                            <Text style={[styles[index], styles.text]}>{index + 1}. {item.name}</Text>
                            <Text style={[styles[index], styles.text]}>{item.score}</Text>
                        </View>
                    );
                }}
            />
        </View>
    );
}

export default Score;
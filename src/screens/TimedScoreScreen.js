import { View } from "react-native";
import Score from "../components/Score"
import { useEffect, useState } from "react";
import { getScoreboard } from "../utils";

const NormalScoreScreen = ({route, navigation}) => {
    const [players, setPlayers] = useState([]);

    useEffect(() => {
        async function fetchPlayers(){
            let scoreboard = await getScoreboard(true); 
            setPlayers(scoreboard);
        }

        fetchPlayers();
    }, []);

    return(
        <View>
            <Score items={players}/>
        </View>
    );    
}

export default NormalScoreScreen;
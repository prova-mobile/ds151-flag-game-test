import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import TimedScoreScreen from './TimedScoreScreen';
import NormalScoreScreen from './NormalScoreScreen';

const Tab = createMaterialTopTabNavigator()

const ScoreboardScreen = ({route, navigation}) => {
    return(
        <Tab.Navigator>
            <Tab.Screen name="Normal" component={NormalScoreScreen}/>
            <Tab.Screen name="Por tempo" component={TimedScoreScreen}/>
        </Tab.Navigator>
    );
};

export default ScoreboardScreen;
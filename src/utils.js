import axios from 'axios';

const timedUrl = "http://200.236.3.126:9999/scoreboards/20205176/timedscores"
const standardUrl = "http://200.236.3.126:9999/scoreboards/20205176/scores";

async function addScore(name, score, timed){
    let url = standardUrl;
    if(timed){
        url = timedUrl;
    }

    let data = {name, score};
    console.log(data);
    await axios
        .post(url, data)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error)
        });
}

async function getScoreboard(timed){
    let url = standardUrl;
    if(timed){
        url = timedUrl;
    }

    let players = axios
        .get(url)
        .then(response => response.data)
        .catch(error => console.log(error));

    console.log(players);
    return players;
}

export { addScore, getScoreboard };
